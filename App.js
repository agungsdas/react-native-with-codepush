import 'react-native-gesture-handler'
import * as React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import codePush from 'react-native-code-push'
import Routes from './src/routes'
import Notfound from './src/components/Notfound'

const App = () => {
  return (
    <NavigationContainer
      fallback={<Notfound title="Tunggu sebentar" icon="cog" />}>
      <Routes />
    </NavigationContainer>
  )
}

export default codePush({
  updateDialog: {
    appendReleaseDescription: true,
    descriptionPrefix: '\n\nChange log:\n',
  },
  checkFrequency: codePush.CheckFrequency.ON_APP_RESUME,
  installMode: codePush.InstallMode.IMMEDIATE,
  mandatoryInstallMode: codePush.InstallMode.IMMEDIATE,
})(App)
