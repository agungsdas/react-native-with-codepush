module.exports = {
  root: true,
  extends: '@react-native-community',
  plugins: [
    "require-path-exists"
  ],
  rules: {
    "react-native/no-inline-styles": 0,
    "react/react-in-jsx-scope": 0,
    "require-path-exists/exists": 2,
    semi: [2, "never"]
  }
};
