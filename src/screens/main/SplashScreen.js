import React, { useEffect, useCallback } from 'react'
import { Image, SafeAreaView, StatusBar, Text } from 'react-native'
import colors from '../../styles/colors'

const SplashScreen = ({ navigation }) => {
  const navigateScreen = useCallback(() => {
    setTimeout(() => {
      navigation.replace('MainScreen')
    }, 2000)
  }, [navigation])

  useEffect(() => {
    navigateScreen()
  }, [navigateScreen])

  return (
    <SafeAreaView
      style={{
        alignSelf: 'stretch',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <StatusBar
        barStyle="light-content"
        backgroundColor={colors.primaryColor}
      />
      <Image
        source={require('../../assets/images/logo.png')}
        style={{ width: 150, height: 150 }}
      />
      <Text style={{ fontWeight: '500', fontSize: 22 }}>Fabelio</Text>
    </SafeAreaView>
  )
}

export default SplashScreen
