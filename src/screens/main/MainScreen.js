import React from 'react'
import { Text, SafeAreaView, StatusBar, Image } from 'react-native'
import colors from '../../styles/colors'

const MainScreen = () => {
  return (
    <SafeAreaView
      style={{
        alignSelf: 'stretch',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <StatusBar
        barStyle="light-content"
        backgroundColor={colors.primaryColor}
      />
      <Image
        source={require('../../assets/images/logo.png')}
        style={{ width: 150, height: 150 }}
      />
      <Text>Fabelio Talks "React Native with Codepush"</Text>
    </SafeAreaView>
  )
}

export default MainScreen
