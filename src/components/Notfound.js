import React from 'react'
import PropTypes from 'prop-types'
import { View, Text } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import colors from '../styles/colors'

const Notfound = ({ title = null, subTitle = null, icon = null }) => {
  return (
    <View
      style={{
        alignSelf: 'stretch',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      {icon && (
        <Icon
          name={icon}
          color={colors.primaryColor}
          size={40}
          style={{ marginBottom: 10 }}
        />
      )}
      {title && (
        <Text
          style={{
            color: colors.primaryColor,
            fontSize: 16,
          }}>
          {title}
        </Text>
      )}
      {subTitle && (
        <Text style={{ color: 'grey', marginTop: 5 }}>{subTitle}</Text>
      )}
    </View>
  )
}

Notfound.propTypes = {
  title: PropTypes.string,
  subTitle: PropTypes.string,
  icon: PropTypes.string,
}

export default Notfound
