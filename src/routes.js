import { createStackNavigator } from '@react-navigation/stack'
import * as React from 'react'
import SplashScreen from './screens/main/SplashScreen'
import MainScreen from './screens/main/MainScreen'

const Stack = createStackNavigator()

const Routes = () => {
  return (
    <Stack.Navigator initialRouteName="SplashScreen">
      <Stack.Screen
        name="SplashScreen"
        component={SplashScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="MainScreen"
        component={MainScreen}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  )
}

export default Routes
